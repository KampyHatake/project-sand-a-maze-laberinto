// 1: Avanza, 2: Derecha, 3: Izquierda
void checkWalls(byte pdata[]){
  // Si no hay pared almacena el valor de la casilla adyacente
  if(!wallLeft()){
    switch (direccion) {
      case 1:
        pdata[0] = flood_fill[x-1][y];
        break;
      case 2:
        pdata[0] = flood_fill[x][y+1];
        break;
      case 3:
        pdata[0] = flood_fill[x+1][y];
        break;
      default:
        pdata[0] = flood_fill[x][y-1];
        break;
    }
  }
  if(!wallRight()){
    switch (direccion) {
      case 1:
        pdata[1] = flood_fill[x+1][y];
        break;
      case 2:
        pdata[1] = flood_fill[x][y-1];
        break;
      case 3:
        pdata[1] = flood_fill[x-1][y];
        break;
      default:
        pdata[1] = flood_fill[x][y+1];
        break;
    }           
  }
  if(!wallFront()){
    switch (direccion) {
      case 1:
        pdata[2] = flood_fill[x][y+1];
        break;
      case 2:
        pdata[2] = flood_fill[x+1][y];
        break;
      case 3:
        pdata[2] = flood_fill[x][y-1];
        break;
      default:
        pdata[2] = flood_fill[x-1][y];
        break;
    }
  }
}

byte updateWalls(byte pdata[]){
  // El valor de la casilla actual es = 1 + el mínimo de las vecinas (sin pared)?
  //   Sí: Avanza
  //   No: Actualiza y avanza
  if (pdata[2] <= pdata[1] and pdata[2] <= pdata[0]){
    if(flood_fill[x][y] != 1 + pdata[2])
      flood_fill[x][y] = 1 + pdata[2];
    return 1;
  }
  else if (pdata[1] <= pdata[2] and pdata[1] <= pdata[0]){
    if(flood_fill[x][y] != 1 + pdata[1])
      flood_fill[x][y] = 1 + pdata[1];
    return 2;
  }
  else {
    if(flood_fill[x][y] != 1 + pdata[0])
      flood_fill[x][y] = 1 + pdata[0];
    return 3;
  }
}

void updateDirection(bool turn){
  if(turn)
    if(direccion >= 4)
      direccion = 1;
    else
      ++direccion;
  else
    if(direccion <= 1)
      direccion = 4;
    else
      --direccion;
}

void updatePosition(){
  if(direccion == 1)
    ++y;
  else if(direccion == 2)
    ++x;
  else if(direccion == 3)
    --y;
  else
    --x;
}
