// Tiempos de giro
  // Adelante/Atras
  #define CELL 1075UL
  // Desde parado
  #define TURN90R1 1400UL
  #define TURN90R2 800UL
  #define TURN90R3 360UL
  #define TURN90R4 0UL
  #define TURN90L1 1500UL
  #define TURN90L2 800UL
  #define TURN90L3 360UL
  #define TURN90L4 0UL
  // 180
  #define TURN180R 790UL
  #define TURN180L 785UL
  // 360
  #define TURN360R 1630UL
  #define TURN360L 1615UL
  // En movimiento
  #define TURN180R2 870UL
  #define TURN180L2 850UL

// Velocidad
  #define BASE_SPEED 255

/*
  Giros:
    1. La rueda interior gira más despacio
    2. La rueda interior no gira
    3. La rueda interior gira en sentido contrario (despacio)
    4. La rueda interior gira en sentido contrario
*/

void moveForward(){ motorControl(BASE_SPEED, BASE_SPEED, 0, 0);}
void moveRight1() { motorControl(BASE_SPEED, BASE_SPEED/2, 0, 0);}
void moveRight2() { motorControl(BASE_SPEED, 0, 0, 0);}
void moveRight3() { motorControl(BASE_SPEED, 0, 0, BASE_SPEED/2);}
void moveRight4() { motorControl(BASE_SPEED, 0, 0, BASE_SPEED);}
void moveLeft1()  { motorControl(BASE_SPEED/2, BASE_SPEED, 0, 0);}
void moveLeft2()  { motorControl(0, BASE_SPEED, 0, 0);}
void moveLeft3()  { motorControl(0, BASE_SPEED/2, BASE_SPEED, 0);}
void moveLeft4()  { motorControl(0, BASE_SPEED, BASE_SPEED, 0);}
void moveBack()   { motorControl(0, 0, BASE_SPEED, BASE_SPEED);}
void botStop()    { motorControl(0, 0, 0, 0);}


/*
  unsigned long turnTimer = 0UL;
  
    turnTimer = millis() + CELL;
    while(millis() <= turnTimer) {
      bot_foward();
    }
    turnTimer = millis() + CELL;
    while(millis() <= turnTimer) {
      bot_foward();
    }

    turnTimer = millis() + TURN90;
    while(millis() <= turnTimer) {
      bot_left();
    }

    turnTimer = millis() + TURN180;
    while(millis() <= turnTimer) {
      bot_spin_left();
    }

  for(int i=0; i<2; i++){
    turnTimer = millis() + CELL;
    while(millis() <= turnTimer)
      bot_foward();
  }

    turnTimer = millis() + TURN180R;
    while(millis() <= turnTimer) {
      bot_spin_right();
    }

    turnTimer = millis() + 700UL;
    while(millis() <= turnTimer)
      bot_foward();

    turnTimer = millis() + CELL;
    while(millis() <= turnTimer)
      bot_foward();*/
