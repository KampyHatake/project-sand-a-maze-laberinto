// Alberto Ferreiro Campello "Kampy"
// Yago Rey Viñas "James"
// Proyecto Sandía
// 3 IR SHARP 0A51SK F 2Z
// http://oshwdem.org


////////////////////////////////// Optimización & librerías ///////////////////////////////


// Flags de optimización, poner esta línea al inicio del programa

  #pragma GCC optimize("-O2")
/*  -O0 Sin optimizar, compilación rápida.
 *  -O1 Optimizacion moderada; no retrasa mucho el tiempo de compilacion.
 *  -O2 Optimizacion completa; tarda más en compilar.
 *  -O3 Igual que -O2; más agresivo e intenta deshacer los bucles.
 *  -Os Optimiza espacio (codigo y datos).
 */


//////////////////// Variables globales & Declaración de constantes ////////////////////////


// Control de errores
  #define DEBUG false

// Posición dentro del laberinto
  volatile byte x = 0;
  volatile byte y = 0;

// Dirección (norte/arriba 1, este/derecha 2, sur/abajo 3, oeste/izquierda 4)
  volatile byte direccion = 1;

// Mapa del laberinto (16x16)
  volatile byte flood_fill[16][16]={
    {14,13,12,11,10, 9, 8, 7, 7, 8, 9,10,11,12,13,14},
    {13,12,11,10, 9, 8, 7, 6, 6, 7, 8, 9,10,11,12,13},
    {12,11,10, 9, 8, 7, 6, 5, 5, 6, 7, 8, 9,10,11,12},
    {11,10, 9, 8, 7, 6, 5, 4, 4, 5, 6, 7, 8, 9,10,11},
    {10, 9, 8, 7, 6, 5, 4, 3, 3, 4, 5, 6, 7, 8, 9,10},
    { 9, 8, 7, 6, 5, 4, 3, 2, 2, 3, 4, 5, 6, 7, 8, 9},
    { 8, 7, 6, 5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 7, 8},
    { 7, 6, 5, 4, 3, 2, 1, 0, 0, 1, 2, 3, 4, 5, 6, 7},
    { 7, 6, 5, 4, 3, 2, 1, 0, 0, 1, 2, 3, 4, 5, 6, 7},
    { 8, 7, 6, 5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 7, 8},
    { 9, 8, 7, 6, 5, 4, 3, 2, 2, 3, 4, 5, 6, 7, 8, 9},
    {10, 9, 8, 7, 6, 5, 4, 3, 3, 4, 5, 6, 7, 8, 9,10},
    {11,10, 9, 8, 7, 6, 5, 4, 4, 5, 6, 7, 8, 9,10,11},
    {12,11,10, 9, 8, 7, 6, 5, 5, 6, 7, 8, 9,10,11,12},
    {13,12,11,10, 9, 8, 7, 6, 6, 7, 8, 9,10,11,12,13},
    {14,13,12,11,10, 9, 8, 7, 7, 8, 9,10,11,12,13,14}
  };


///////////////////////////////// Inicialización de variables ///////////////////////////////


void setup()
{
  // Inicializacion de pines (Equivale a multiples pinMode), 1 = salida, 0 = entrada
  DDRD |= B01100000; // Digital 0~7;  Pines 5 y 6 como salida
  DDRB |= B00100110; // Digital 8~13; Pines 9, 10 y 13 como salida
  DDRC |= B00000000; // Analog A0~A8; Todos de entrada

  if(DEBUG){
    Serial.begin(115200);
  }

  delay(1000);
}


/////////////////////////////////////// Bucle principal /////////////////////////////////////


void loop()
{
  byte wall = 0;
  byte data[3] = {254, 254, 254};

  // Do stuff
  PORTB &= ~B00100000; // Onboard LED 13 OFF
  while(1)
  {
    checkWalls(data);
    wall = updateWalls(data);
    // Gira
    switch (wall) {
      case 2:
        moveRight4();
        updateDirection(true);
        break;
      case 3:
        moveLeft4();
        updateDirection(false);
        break;
    }
    // Muro
    while(wallFront())
      moveRight4();
      updateDirection(true);
    // Avanza
    moveForward();
    updatePosition();
    // Meta
    if(flood_fill[x][y] == 0)
      break;
  }

  // Stop
  PORTB |= B00100000; // Onboard LED 13 ON
  while(1)
    botStop();
}
