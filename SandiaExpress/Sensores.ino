// Sensores
  #define LEFT_SENSOR A4
  #define RIGHT_SENSOR A0
  #define FRONT_SENSOR A2
  #define GOAL_SENSOR A5

bool wallFront() {
  if(analogRead(FRONT_SENSOR))
    return true;
  else
    return false;
}

bool wallRight() {
  if(analogRead(RIGHT_SENSOR))
    return true;
  else
    return false;
}

bool wallLeft() {
  if(analogRead(LEFT_SENSOR))
    return true;
  else
    return false;
}

bool goal() {
  if(analogRead(GOAL_SENSOR))
    return true;
  else
    return false;
}
