// Motores
  #define BACK_LEFT 10
  #define FRONT_LEFT 5
  #define BACK_RIGHT 6
  #define FRONT_RIGHT 9

void motorControl(byte fLeft, byte fRight, byte bLeft, byte bRight){
  analogWrite (FRONT_LEFT, fLeft);
  analogWrite (BACK_LEFT, bLeft);
  analogWrite (FRONT_RIGHT, fRight);
  analogWrite (BACK_RIGHT, bRight);
}
